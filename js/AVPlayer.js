(function(){
	
	var AVPlayer = {
		url: null,
		videoObj: null,	// tag video
		STATES: {
			NONE: -1,
	        STOPPED: 0,
	        PLAYING: 1,
	        PAUSED: 2
	    },
	    SKIP_STATES: {
	    	NONE: -1,
	    	FORWARD: 0,
	    	REWIND: 1
	    },
	    state: -1,		// initial state: NONE
	    skipState : -1,
	    isFullscreen : false,
	    defaultResolutionWidth : 1920,
	    resolutionWidth : 0,
	    playerCoords : null
	};
	
	// Initialize player
	AVPlayer.init = function (id) {
		
		console.log("Player.init("+id+")");
		console.log("AVPlay version: " + webapis.avplay.getVersion());
		
		if (!this.videoObj && id) {
			this.videoObj = document.getElementById(id);
		}
	}
	
	AVPlayer.setVideoURL = function(url) {
	    this.url = url;
	    console.log("URL = " + this.url);   		
	}
	
	// Play video
	AVPlayer.play = function () {
		
		if (webapis.avplay.getState() === 'NONE' || webapis.avplay.getState() === 'IDLE') {
		
		    console.log("Player.prepare("+this.url+")");
		
		    if (!this.videoObj) {
			    return 0;
		    }
		
		    webapis.avplay.open(this.url);
		    this.setupEventListeners();
		    this.setDisplayArea(458, 63, 473, 267);
		    //webapis.avplay.setStreamingProperty("SET_MODE_4K") //for 4K contents			
		    webapis.avplay.prepare();
		
		    Display.setTotalTime(webapis.avplay.getDuration());
		  
		    console.log("Player.play("+this.url+")");
		
		    this.state = this.STATES.PLAYING;
		
		    if (this.url) {
			    this.videoObj.src = this.url;
		    }
		}
		webapis.avplay.play(); 
	}
	
	AVPlayer.playNextVideo = function() {
	    var m = Main.mute;
	    var mode = Main.mode;
	    Main.selectNextVideo(Main.DOWN);
	    if (Main.selectedVideo === 0) {
	    	Main.fetchVideoList();
	    } else {
	    	AVPlayer.play();
	    }
        if (Main.mute === Main.YMUTE) {
      	    Main.mute = Main.NMUTE;
      	    Main.muteMode();
          }
          if (Main.mode === Main.FULLSCREEN) {
      	    Main.mode = Main.WINDOW;
      	    Main.toggleMode();
          }
	}
	
	// Pause video
	AVPlayer.pause = function () {
		
		console.log("Player.pause()");
		
		if (this.state !== this.STATES.PLAYING) {
			return;
		}
		
		this.state = this.STATES.PAUSED;
		
		webapis.avplay.pause();
				
	}
	
	// Stop video
	AVPlayer.stop = function () {
		
		console.log("Player.stop()");
		
		this.state = this.STATES.STOPPED;		
		webapis.avplay.stop();
	}
	
	// Set position and dimension of video area 
	AVPlayer.setDisplayArea = function (x, y, width, height) {		
		webapis.avplay.setDisplayRect(x,y,width,height);
	}
	
	// format time in seconds to hh:mm:ss
	
    /**
     * Jump forward 3 seconds (3000 ms).
     */
	AVPlayer.ff = function () {
		this.skipState = AVPlayer.SKIP_STATES.FORWARD;
        webapis.avplay.jumpForward('3000');
    }
    /**
     * Rewind 3 seconds (3000 ms).
     */
	AVPlayer.rew = function () {
    	this.skipState = AVPlayer.SKIP_STATES.REWIND;
        webapis.avplay.jumpBackward('3000');
    }
	
	AVPlayer.setCurTime = function(time) {
		Display.setTime(time);
	}

	
	// Setup Listeners for video player events
	AVPlayer.setupEventListeners = function () {
	
		var that = this;
		
		var listener = {
			onbufferingstart: function() {
	    		Display.status("Buffering...");
	    	    switch(this.skipState) {
	    	        case AVPlayer.SKIP_STATES.FORWARD:
	    	            document.querySelector(".ff").style.opacity = '0.2';
	    	            break;
	    	        
	    	        case AVPlayer.SKIP_STATES.REWIND:
	    	            document.querySelector(".rew").style.opacity = '0.2';
	    	            break;
	    	    }
			},
			onbufferingprogress: function(percent) {
			    Display.status("Buffering:" + percent + "%");
			},
			onbufferingcomplete: function() {
			    Display.status("Play");
			    switch(this.skipState) {
			        case AVPlayer.SKIP_STATES.FORWARD:
			            document.getElementById("ff").style.opacity = '1.0';
			            break;
			        
			        case AVPlayer.SKIP_STATES.REWIND:
			            document.getElementById("rew").style.opacity = '1.0';
			            break;
			    }
			},
			onstreamcompleted: function() {
				AVPlayer.playNextVideo();
			},
			oncurrentplaytime: function(time) {
				AVPlayer.setCurTime(webapis.avplay.getCurrentTime());
			},
			ondrmevent: function(drmEvent, drmData) {
				console.log("DRM callback: " + drmEvent + ", data: " + drmData);
			},			
			onerror : function(type, data) {
				console.log("OnError: " + data);
			}
	    }
		
		webapis.avplay.setListener(listener);
        
	}	
	
	if (!window.AVPlayer) {
		window.AVPlayer = AVPlayer;
	}	
	
})()