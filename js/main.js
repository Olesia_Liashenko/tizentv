var Main =
{
    selectedVideo : 0,
    mode : 0,
    mute : 0,
    
    UP : 0,
    DOWN : 1,

    WINDOW : 0,
    FULLSCREEN : 1,
    
    NMUTE : 0,
    YMUTE : 1,
    videoListUrl : "http://10.17.131.4/tvinformer/videolist.xml"
};

//enabling media keys
Main.registerKeys = function() {

    var usedKeys = [
        'MediaPause',
        'MediaPlay',
        'MediaPlayPause',
        'MediaFastForward',
        'MediaRewind',
        'MediaStop',
        'VolumeUp',
        'VolumeDown',
        'VolumeMute'
    ];

    usedKeys.forEach(
        function (keyName) {
            tizen.tvinputdevice.registerKey(keyName);
        }
    );
};

//handle all keydown events triggered through remote control.
Main.handleKeyDownEvents = function () {

	// add eventListener for keydown
    document.addEventListener('keydown', function (e) {
        switch (e.keyCode) {
            case 13:    // Enter
            case 10171: // KEY_PANEL_ENTER
            	Main.toggleMode();
                break;
            case 10252: // MediaPlayPause
			    console.log("PLAYPAUSE");
    		    if (AVPlayer.state === AVPlayer.STATES.PLAYING) {
    			    AVPlayer.pause();
    		    } else {
    			    AVPlayer.play();
    		    }    		
    		    break;
            case 415:   // MediaPlay
    		    console.log("PLAY");
    		    AVPlayer.play();
    		    break;
            case 19:    // MediaPause
    		    console.log("PAUSE");
    		    AVPlayer.pause();
    		    break;
            case 38: // Up
            	Main.selectPreviousVideo(Main.UP);
            	break;
            case 40: //Down
            	Main.selectNextVideo(Main.DOWN);
            	break;
            case 413:   // MediaStop
                AVPlayer.stop();
                break;
            case 417:   // MediaFastForward
			    if (AVPlayer.state !== AVPlayer.STATES.PAUSED) {
                    AVPlayer.ff();
			    }
                break;
            case 412:   // MediaRewind
			    if (AVPlayer.state !== AVPlayer.STATES.PAUSED) {
                    AVPlayer.rew();
				}
                break;
            case 447: // KEY_VOL_UP
            	if (!tizen.tvaudiocontrol.isMute()) {
            		tizen.tvaudiocontrol.setVolumeUp();
            	}
                break;
            case 448: // KEY_VOL_DOWN
            	if (!tizen.tvaudiocontrol.isMute()) {
            		tizen.tvaudiocontrol.setVolumeDown();
            	}            	
            	break;
            case 449: // KEY_MUTE
            	if (tizen.tvaudiocontrol.isMute()) {
            		tizen.tvaudiocontrol.setMute(false);
            	} else {
            		tizen.tvaudiocontrol.setMute(true);
            	}
            	break;
            case 10009: // Return
                if (webapis.avplay.getState() !== 'IDLE' && webapis.avplay.getState() !== 'NONE') {
                    AVPlayer.stop();
                } else {
                    tizen.application.getCurrentApplication().hide();
                }
                break;
            default:
                console.log("Unhandled key");
        }
    });
};

Main.fetchVideoList = function() {
	var req = new tizen.DownloadRequest(this.videoListUrl);
	var listener = {
	      oncompleted: function(id, fullPath) {
	    		tizen.filesystem.resolve('downloads',
	    				function (dir) {
	    			    var file = dir.resolve("videolist.xml");
	    				file.readAsText(function(str){
	    	            	var xmlDoc = (new DOMParser()).parseFromString(str, 'text/xml');
	    	                var items = xmlDoc.getElementsByTagName("item");
	    	                
	    	                var videoNames = [ ];
	    	                var videoURLs = [ ];
	    	                var videoDescriptions = [ ];
	    	                
	    	                for (var index = 0; index < items.length; index++) {
	    	                    var titleElement = items[index].getElementsByTagName("title")[0];
	    	                    var descriptionElement = items[index].getElementsByTagName("description")[0];
	    	                    var linkElement = items[index].getElementsByTagName("link")[0];
	    	                    if (titleElement && descriptionElement && linkElement)
	    	                    {
	    	                    	videoNames[index] = "";
	    	                    	if (titleElement.firstChild) {
	    	                        	videoNames[index] = titleElement.firstChild.data;
	    	                        }
	    	                       
	    	                        if(linkElement.firstChild.data.substring(0,4) !=="http"){
	    	                        	console.log("asdasdasd  "+linkElement.firstChild.data.substring(0,4));
	    	                        	var rootPath = window.location.href.substring(0, location.href.lastIndexOf("/")+1);
	    	                	    	var Abs_path = decodeURI(rootPath).split("file://")[1]+linkElement.firstChild.data;
	    	                	    	videoURLs[index] = Abs_path;  	   	
	    	                	    }
	    	                        else{
	    	                        	videoURLs[index] = linkElement.firstChild.data;                        	
	    	                        }
	    	                        videoDescriptions[index] = "";
	    	                        if (descriptionElement.firstChild) {
	    	                        	videoDescriptions[index] = descriptionElement.firstChild.data;
	    	                        }
	    	                    }
	    	                }
	    	                dir.deleteFile(file.fullPath, function(){console.log("VideoList was deleted succesfully!");},
	    	                		function(e){console.log("Error: " + e.message);});
	    	                
	    	                Data.setVideoNames(videoNames);
	    	                Data.setVideoURLs(videoURLs);
	    	                Data.setVideoDescriptions(videoDescriptions);
	    	                console.log("videoNames: " + videoNames);
	    	                console.log("videoURLs: " + videoURLs);
	    	                console.log("videoDescriptions: " + videoDescriptions);
	    	                Display.setVideoList( Data.getVideoNames() );
	    	                
	    	                Main.updateCurrentVideo();

	    	                AVPlayer.play();
	    	                
	    	            });
	    		});
	      },
	      onfailed: function(id, error) {
	          console.log('Failed with id: ' + id + ', error name: ' + error.name);
	      }
	  };
    tizen.download.start(req, listener);

}

Main.updateCurrentVideo = function(move) {
	
	AVPlayer.setVideoURL( Data.getVideoURL(this.selectedVideo) );
    
    Display.setVideoListPosition(this.selectedVideo, move);

    Display.setDescription( Data.getVideoDescription(this.selectedVideo));
}

Main.selectNextVideo = function(down) {
	AVPlayer.stop();
    
    this.selectedVideo = (this.selectedVideo + 1) % Data.getVideoCount();

    this.updateCurrentVideo(down);
}

Main.selectPreviousVideo = function(up) {
	AVPlayer.stop();
    
    if (--this.selectedVideo < 0) {
        this.selectedVideo += Data.getVideoCount();
    }

    this.updateCurrentVideo(up);
}


Main.toggleMode = function() {
	
	if (AVPlayer.state === AVPlayer.STATES.PAUSED) {
		AVPlayer.play();
	}
    if (AVPlayer.isFullscreen === false) {
        webapis.avplay.setDisplayRect(0, 0, 1920, 1080);
        AVPlayer.isFullscreen = true;
    } else {
        console.log('Fullscreen off');
        try {
            webapis.avplay.setDisplayRect(
            		AVPlayer.playerCoords.x,
            		AVPlayer.playerCoords.y,
            		AVPlayer.playerCoords.width,
            		AVPlayer.playerCoords.height
            );
        } catch (e) {
            console.log(e.message);
        }
        AVPlayer.isFullscreen = false;
    }
}

Main.muteMode = function() {
    switch (this.mute) {
        case this.NMUTE:
            this.setMuteMode();
            break;
            
        case this.YMUTE:
            this.noMuteMode();
            break;
            
        default:
            alert("ERROR: unexpected mode in muteMode");
            break;
    }
}

Main.setMuteMode = function() {
    if (!tizen.tvaudiocontrol.isMute()) {
        tizen.tvaudiocontrol.setMute(true);
        document.getElementById("volumeInfo").innerHTML = "MUTE";
        document.getElementById("volumeBar").style.backgroundImage = "url(images/videoBox/muteBar.png)";
        document.getElementById("volumeIcon").style.backgroundImage = "url(images/videoBox/mute.png)";
        this.mute = this.YMUTE;
    }
}

Main.noMuteMode = function() {
    if (tizen.tvaudiocontrol.isMute()) {
    	tizen.tvaudiocontrol.setMute(false);
    	document.getElementById("volumeBar").style.backgroundImage = "url(images/videoBox/volumeBar.png)";
    	document.getElementById("volumeIcon").style.backgroundImage = "url(images/videoBox/volume.png)";
        Display.setVolume( tizen.tvaudiocontrol.getVolume() );
        this.mute = this.NMUTE;
    }
}

Main.onLoad = function () {
	console.log("Main.onLoad()");
	
	//enabling media keys
	Main.registerKeys();
	
	// setup handler to key events
	Main.handleKeyDownEvents();
	
	// setup video player
	AVPlayer.init("av-player");
	Display.init();
	
	Main.fetchVideoList();

	tizen.systeminfo.getPropertyValue(
            "DISPLAY",
            function (display) {
                console.log("The display width is " + display.resolutionWidth);
                AVPlayer.resolutionWidth = display.resolutionWidth;
                AVPlayer.playerCoords = {
        	            x: Math.floor(10 * display.resolutionWidth / AVPlayer.defaultResolutionWidth),
        	            y: Math.floor(300 * display.resolutionWidth / AVPlayer.defaultResolutionWidth),
        	            width: Math.floor(854 * display.resolutionWidth / AVPlayer.defaultResolutionWidth),
        	            height: Math.floor(480 * display.resolutionWidth / AVPlayer.defaultResolutionWidth)
        		};
            	

            	
            	//AVPlayer.prepare("http://yourvideourl.mp4"); // <-- set video URL here!
            	
            },
            function(error) {
                console.log("An error occurred " + error.message);
            }
        );

    
}


// window.onload can work without <body onload="">
window.onload = Main.onLoad;